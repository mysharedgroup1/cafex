package com.capgemini.cafex.billing;

import com.capgemini.cafex.model.Invoice;
import com.capgemini.cafex.model.Order;

public interface BillingService {
	
	public Invoice createInvoice(Order order);
	
	public Invoice getInvoice(String id);
	
}
