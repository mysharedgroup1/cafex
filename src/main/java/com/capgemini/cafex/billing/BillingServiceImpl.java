package com.capgemini.cafex.billing;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.capgemini.cafex.catalog.CatalogService;
import com.capgemini.cafex.model.Invoice;
import com.capgemini.cafex.model.InvoiceLineItem;
import com.capgemini.cafex.model.OCC;
import com.capgemini.cafex.model.OCCApplicationLevel;
import com.capgemini.cafex.model.OCCItem;
import com.capgemini.cafex.model.Order;
import com.capgemini.cafex.model.OrderLineItem;
import com.capgemini.cafex.model.ProductOffering;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class BillingServiceImpl implements BillingService {
	
	private CatalogService catalogSrv;
	
	public BillingServiceImpl(CatalogService catalogSrv) {
		this.catalogSrv = catalogSrv;
	}

	@Override
	public Invoice createInvoice(Order order) {
		// some basic validations
		List<OrderLineItem> orderLineItems = null;
		if (null == order || null == (orderLineItems = order.getOrderLineItems()) || orderLineItems.isEmpty()) {
			throw new IllegalArgumentException("The order must contain at least an item to be able to generate an invoice");
		}
		final Invoice invoice = new Invoice(order, new Date());
		Set<String> duplicates = new HashSet<String>();
		for (OrderLineItem orderItem : orderLineItems) {
			if (null == orderItem) {
				throw new IllegalArgumentException("Order items are not allowed to be empty");
			}
			ProductOffering po = null;
			InvoiceLineItem lineItem = new InvoiceLineItem(
					orderItem.getLineItemId(), 
					po = this.catalogSrv.getProductOffering(orderItem.getProductOfferingId()), 
					this.catalogSrv.getProductOfferingPrice(orderItem.getProductOfferingId()), 
					orderItem.getQuantity());
			invoice.addInvoiceLineItem(lineItem);
			final List<OCC> occs = this.catalogSrv.getEligibleOCCs(po, null);
			for (OCC occ : occs) {
				if (duplicates.contains(occ.getOccId()) && occ.isSingleApplication()) {
					continue;
				}
				if (occ.getApplicationLevel() == OCCApplicationLevel.ORDER_LINE_ITEM) {
					invoice.addOccItem(new OCCItem(occ, lineItem.getLineItemTotal(), lineItem.getOrderLineItemId()));
				} else {
					invoice.addOccItem(new OCCItem(occ));
				}
				duplicates.add(occ.getOccId());
			}
		}
		invoice.calculateTotals();
		return invoice;
	}

	@Override
	public Invoice getInvoice(String id) {
		throw new NotImplementedException();
	}
	

}
