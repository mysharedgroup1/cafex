package com.capgemini.cafex.model;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class Invoice {
	
	private String invoiceId;
	private String orderId;
	private Date invoiceDate;
	
	/** total excluding other credits and charges */
	private double subTotal = 0d;
	
	/** total after applying other credits and charges */
	private double total = 0d;
	
	private List<InvoiceLineItem> invoiceLineItems = new LinkedList<InvoiceLineItem>();
	private List<OCCItem> occItems = new LinkedList<OCCItem>();
	
	public Invoice(Order order, Date invoiceDate) {
		this.invoiceId = UUID.randomUUID().toString();
		this.invoiceDate = invoiceDate;
		this.orderId = order.getOrderId();
	}
	
	public void calculateTotals() {
		this.total = this.subTotal;
		for (OCCItem item : occItems) {
			// invoice level
			if (null == item.getInvoiceLineItemId()) {
				item.setReferenceAmount(this.subTotal);
			} 
			// may actually over discount
			this.total += item.getCalculatedAmount();
		}
	}
	
	public String getOrderId() {
		return orderId;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public List<InvoiceLineItem> getInvoiceLineItems() {
		return invoiceLineItems;
	}

	public void addInvoiceLineItem(InvoiceLineItem invoiceLineItem) {
		this.invoiceLineItems.add(invoiceLineItem);
		this.subTotal += invoiceLineItem.getLineItemTotal();
	}
	
	public void addOccItem(OCCItem occItem) {
		this.occItems.add(occItem);
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public double getSubTotal() {
		return subTotal;
	}

	public double getTotal() {
		return total;
	}

	public List<OCCItem> getOccItems() {
		return occItems;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder().append(String.format("\n%25s  %5s  %15s  %16s", 
				"Item name", "Quantity", "Price Per Unit", "Total Price"));
		sb.append("\n----------------------------------------------------------------------");
		for (InvoiceLineItem item : this.invoiceLineItems) {
			sb.append(String.format("\n%25s  %5s  %15s $  %15s $", 
					item.getProductOffering().getDescription(), item.getQuantity(), item.getPricePerUnit().getPrice(), item.getLineItemTotal()));
		}
		for (InvoiceLineItem item : this.invoiceLineItems) {
			sb.append(String.format("\n%25s  %5s  %15s $  %15s $", 
					item.getProductOffering().getDescription(), item.getQuantity(), item.getPricePerUnit().getPrice(), item.getLineItemTotal()));
		}
		sb.append("\n----------------------------------------------------------------------");
		for (OCCItem item: this.occItems) {
			sb.append(String.format("\n%25s  %5s  %15s $  %15s $", 
					item.getOcc().getDescription(), 1, item.getCalculatedAmount(), item.getCalculatedAmount()));
		}
		sb.append("\n----------------------------------------------------------------------");
		sb.append(String.format("\n%41sSub Total: %16s $", " ", subTotal));
		sb.append(String.format("\n%41sTotal: %20s $", " ", total));
		return sb.toString();		
	}
	
}
