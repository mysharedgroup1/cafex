package com.capgemini.cafex.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.UUID;

public class OCCItem {
	private String occItemId;
	private String invoiceLineItemId;
	private OCC occ;
	private double calculatedAmount;
	
	public OCCItem(OCC occ) {
		this.occ = occ;
		this.occItemId = UUID.randomUUID().toString();
	}
	
	public OCCItem(OCC occ, double amount, String invoiceLineItemId) {
		this(occ);
		this.invoiceLineItemId = invoiceLineItemId;
		setReferenceAmount(amount);
	}
	
	public double setReferenceAmount(double amount) {
		if (occ.getCalculationType() == OCCCalculationType.ABSOLUTE) {
			this.calculatedAmount = occ.getValue();
		} else {
			this.calculatedAmount = occ.getValue() * amount / 100;
		}
        BigDecimal bd = new BigDecimal(this.calculatedAmount).setScale(2, RoundingMode.HALF_UP);
        this.calculatedAmount = bd.doubleValue();

		if (this.calculatedAmount + amount < 0) {
			this.calculatedAmount = -amount;
		}
		return calculatedAmount;
	}

	public String getOccItemId() {
		return occItemId;
	}

	public OCC getOcc() {
		return occ;
	}

	public double getCalculatedAmount() {
		return calculatedAmount;
	}

	public String getInvoiceLineItemId() {
		return invoiceLineItemId;
	}
	
	
}
