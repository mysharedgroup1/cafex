package com.capgemini.cafex.model;

import java.util.UUID;

/** 
 * Other credits and charges (discounts/fees)
 */
public class OCC {
	
	private String occId;
	private String description;
	
	private OCCApplicationLevel applicationLevel;
	private OCCCalculationType calculationType;
	
	/** negative values denote credits */
	private double value;
	
	/** What part of an order is evaluated for the rule **/
	private Class<?> eligibilityType;
	
	/** Used to match the characteristics of a product */
	private String characteristicName;
	
	/** Used to match the characteristic value of a product */
	private String characteristicValue;
	
	private boolean singleApplication = true;
	
	public OCC(Class<?> eligibilityType, OCCApplicationLevel applicationLevel, OCCCalculationType calculationType, double value) {
		this.occId = UUID.randomUUID().toString();
		this.eligibilityType = eligibilityType;
		this.applicationLevel = applicationLevel;
		this.calculationType = calculationType;
		this.value = value;
	}
	
	public OCC(double value) {
		this(ProductOffering.class, OCCApplicationLevel.ORDER, OCCCalculationType.RELATIVE, value);
	}
	
	public String getOccId() {
		return occId;
	}
	
	
	public Class<?> getEligibilityType() {
		return eligibilityType;
	}
	
	public void setEligibilityType(Class<?> eligibilityType) {
		this.eligibilityType = eligibilityType;
	}

	public OCCApplicationLevel getApplicationLevel() {
		return applicationLevel;
	}
	
	public void setApplicationLevel(OCCApplicationLevel applicationLevel) {
		this.applicationLevel = applicationLevel;
	}
	
	public OCCCalculationType getCalculationType() {
		return calculationType;
	}
	
	public void setCalculationType(OCCCalculationType calculationType) {
		this.calculationType = calculationType;
	}
	
	public String getCharacteristicName() {
		return characteristicName;
	}
	
	public void setCharacteristicName(String characteristicName) {
		this.characteristicName = characteristicName;
	}
	
	public String getCharacteristicValue() {
		return characteristicValue;
	}
	
	public void setCharacteristicValue(String characteristicValue) {
		this.characteristicValue = characteristicValue;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public boolean isSingleApplication() {
		return singleApplication;
	}

	public void setSingleApplication(boolean singleApplication) {
		this.singleApplication = singleApplication;
	}
}
