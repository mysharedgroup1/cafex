package com.capgemini.cafex.model;

import java.util.Date;

public class ProductOfferingPrice {
	
	private String productOfferingId;
	private double price;
	
	// other factors that may influence pricing
	private String storeId;
	private Date refDate = null;

	
	public ProductOfferingPrice(String productOfferingId, double price) {
		this.productOfferingId = productOfferingId;
		this.price = price;
	}
	
	public String getProductOfferingId() {
		return productOfferingId;
	}

	public String getStoreId() {
		return storeId;
	}
	
	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}
	
	public Date getRefDate() {
		return refDate;
	}
	
	public void setRefDate(Date refDate) {
		this.refDate = refDate;
	}
	
	public double getPrice() {
		return price;
	}
}
