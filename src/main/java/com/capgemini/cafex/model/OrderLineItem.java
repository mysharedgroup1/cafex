package com.capgemini.cafex.model;

import java.util.UUID;

public class OrderLineItem {
	
	/** Identifies the line item in the context of the order */
	private String lineItemId;
	
	/** Identifies the offering in the catalog / menu */
	private String productOfferingId;
	
	/** How many product items */
	private int quantity = 1;
	
	public OrderLineItem() {
		super();
		this.lineItemId = UUID.randomUUID().toString();
	}
	
	public OrderLineItem(int quantity, String productOfferingId) {
		this();
		this.quantity = quantity;
		this.productOfferingId = productOfferingId;
	}
	
	public OrderLineItem(String productOfferingId) {
		this(1, productOfferingId);
		this.productOfferingId = productOfferingId;
	}

	public String getLineItemId() {
		return lineItemId;
	}

	public String getProductOfferingId() {
		return productOfferingId;
	}

	public int getQuantity() {
		return quantity;
	}
}
