package com.capgemini.cafex.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class ProductOffering {
	
	public static final String CHAR_CATEGORY 	= "category";
	public static final String CHAR_TEMPERATURE = "temperature";
	
	public static final String VALUE_TEMP_COLD 	= "cold";
	public static final String VALUE_TEMP_HOT 	= "hot";
	
	public static final String VALUE_CAT_DRINK 	= "drink";
	public static final String VALUE_CAT_FOOD 	= "food";
	
	
	/** Identifier of the offering */
	private String productOfferingId;
	
	/** The description of the offering */
	private String description;
	
	/** Generic characteristics of a given offering (color, temperature, anything really) */	
	private Map<String, String> characteristics = new HashMap<String, String>();
	
	public ProductOffering() {
		this.productOfferingId = UUID.randomUUID().toString();
	}
	
	public void setCharacteristic(String key, String value) {
		this.characteristics.put(key, value);
	}
	
	public String getCharacteristic(String key) {
		return this.characteristics.get(key);
	}
	
	public Set<String> getCharacteristicKeySet() {
		return this.characteristics.keySet();
	}
	
	public String getProductOfferingId() {
		return productOfferingId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
