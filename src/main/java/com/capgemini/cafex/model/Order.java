package com.capgemini.cafex.model;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class Order {
	
	private String orderId;
	private List<OrderLineItem> orderLineItems = new LinkedList<OrderLineItem>();
	
	public Order() {
		this.orderId = UUID.randomUUID().toString(); 
	}
	
	/**
	 * Each order has a unique id
	 * @return
	 */
	public String getOrderId() {
		return orderId;
	}
	
	/**
	 * Returns all order items
	 * @return
	 */
	public List<OrderLineItem> getOrderLineItems() {
		return orderLineItems;
	}
	
	/** 
	 * Adds another order item to the order
	 * @param orderLineItem
	 */
	public void addOrderLineItem(OrderLineItem orderLineItem) {
		this.orderLineItems.add(orderLineItem);
	}
	
}
