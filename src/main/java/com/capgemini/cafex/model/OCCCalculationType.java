package com.capgemini.cafex.model;

public enum OCCCalculationType {
	RELATIVE, ABSOLUTE
}
