package com.capgemini.cafex.model;

import java.util.UUID;

public class InvoiceLineItem {
	
	/** Identifies the line item in the context of the invoice */
	private String invoiceItemId;
	
	/** Identifies the order line item for a particular order */
	private String orderLineItemId;
	
	/** Identifies the offering in the catalog / menu */
	private ProductOffering productOffering;
	
	/** The price of the item in the context of the order */ 
	private ProductOfferingPrice pricePerUnit;
	
	/** How many items were purchased */
	private int quantity = 1;
	
	/** The total price accounting for quantity */
	private double lineItemTotal = 0d;
	
	public InvoiceLineItem(String orderLineItemId, ProductOffering productOffering, ProductOfferingPrice pricePerUnit, int quantity) {
		this.invoiceItemId = UUID.randomUUID().toString();
		this.orderLineItemId = orderLineItemId;
		this.productOffering = productOffering;
		this.pricePerUnit = pricePerUnit; 
		this.quantity = quantity;
		this.lineItemTotal = this.pricePerUnit.getPrice() * this.quantity;
	}

	public String getInvoiceItemId() {
		return invoiceItemId;
	}

	public String getOrderLineItemId() {
		return orderLineItemId;
	}

	public ProductOffering getProductOffering() {
		return productOffering;
	}

	public ProductOfferingPrice getPricePerUnit() {
		return pricePerUnit;
	}

	public int getQuantity() {
		return quantity;
	}

	public double getLineItemTotal() {
		return lineItemTotal;
	}
	
}
