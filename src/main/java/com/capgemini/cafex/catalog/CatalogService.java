package com.capgemini.cafex.catalog;

import java.util.List;
import java.util.Map;

import com.capgemini.cafex.model.OCC;
import com.capgemini.cafex.model.ProductOffering;
import com.capgemini.cafex.model.ProductOfferingPrice;

public interface CatalogService {
	
	/**
	 * Registers a new product offering price
	 * @param productOfferingId
	 * @param price
	 */
	void addProductOfferingPrice(ProductOfferingPrice price);
	
	/**
	 * Registers a new product offering along with a price
	 * @param po
	 * @param price
	 */
	void addProductOffering(ProductOffering po, ProductOfferingPrice price);
	
	/**
	 * Retrieves the details of a particular product offering
	 * @param productOfferingId
	 * @return
	 */
	ProductOffering getProductOffering(String productOfferingId);
	
	/**
	 * Retrieves the price of a give offering
	 * @param productOfferingId
	 * @return
	 */
	ProductOfferingPrice getProductOfferingPrice(String productOfferingId);
	
	/**
	 * Registers a new credit or charge
	 * @param occ
	 */
	void addOCC(OCC occ);
	
	/**
	 * Retrieves any OCC is eligible based on the purchase of a product offering with the provided id and the quantity 
	 * @param productOfferingId
	 * @return
	 */
	List<OCC> getEligibleOCCs(ProductOffering productOffering, Map<String, Object> orderContext);
}
