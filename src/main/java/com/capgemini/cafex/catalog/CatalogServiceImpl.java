package com.capgemini.cafex.catalog;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.capgemini.cafex.model.OCC;
import com.capgemini.cafex.model.ProductOffering;
import com.capgemini.cafex.model.ProductOfferingPrice;

/**
 * A rudimentary implementation of the billing catalog service (for the menu items). 
 * Assumes a catalog service propagates the actual prices 
 */
public class CatalogServiceImpl implements CatalogService {
	
	private Map<String, ProductOffering> offers = new HashMap<String, ProductOffering>();
	private Map<String, ProductOfferingPrice> prices = new HashMap<String, ProductOfferingPrice>();
	private Map<Class<?>, Map<String, Map<String, OCC>>> otherCreditsAndCharges = new HashMap<Class<?>, Map<String,Map<String,OCC>>>(); 
	

	@Override
	public void addProductOfferingPrice(ProductOfferingPrice price) {
		this.prices.put(price.getProductOfferingId(), price);
	}

	@Override
	public void addProductOffering(ProductOffering po, ProductOfferingPrice price) {
		this.offers.put(po.getProductOfferingId(), po);
		this.addProductOfferingPrice(price);
	}

	@Override
	public ProductOffering getProductOffering(String productOfferingId) {
		if (!this.offers.containsKey(productOfferingId)) {
			throw new IllegalArgumentException("Unknown offering");
		}
		return this.offers.get(productOfferingId);
	}

	@Override
	public ProductOfferingPrice getProductOfferingPrice(String productOfferingId) {
		if (!this.offers.containsKey(productOfferingId) || !this.prices.containsKey(productOfferingId)) {
			throw new IllegalArgumentException("Unknown offering");
		}
		return this.prices.get(productOfferingId);
	}

	@Override
	public void addOCC(OCC occ) {
		Map<String, Map<String, OCC>> node = this.otherCreditsAndCharges.get(occ.getEligibilityType());
		if (null == node) {
			this.otherCreditsAndCharges.put(occ.getEligibilityType(), node = new HashMap<String, Map<String, OCC>>());
		}
		Map<String, OCC> leaf = node.get(occ.getCharacteristicName());
		if (null == leaf) {
			node.put(occ.getCharacteristicName(), leaf = new HashMap<String, OCC>());
		}
		leaf.put(occ.getCharacteristicValue(), occ);
	}

	@Override
	public List<OCC> getEligibleOCCs(ProductOffering productOffering, Map<String, Object> orderContext) {
		// determine what rules "fire"
		List<OCC> matched = new LinkedList<OCC>();
		Map<String, Map<String, OCC>> node = this.otherCreditsAndCharges.get(productOffering.getClass());
		
		// no rules configured for this type
		if (null == node || node.isEmpty()) {
			return matched;
		}
		
		// check rules that match
		for (String key : productOffering.getCharacteristicKeySet()) {
			if (node.containsKey(key)) {
				Map<String, OCC> leaf = node.get(key);
				if (null != leaf && leaf.containsKey(productOffering.getCharacteristic(key))) {
					matched.add(leaf.get(productOffering.getCharacteristic(key)));
				}
			}
		}
		return matched;
	}
}
