package com.capgemini.cafex;

import com.capgemini.cafex.billing.BillingService;
import com.capgemini.cafex.billing.BillingServiceImpl;
import com.capgemini.cafex.catalog.CatalogService;
import com.capgemini.cafex.catalog.CatalogServiceImpl;
import com.capgemini.cafex.model.Invoice;
import com.capgemini.cafex.model.OCC;
import com.capgemini.cafex.model.Order;
import com.capgemini.cafex.model.OrderLineItem;
import com.capgemini.cafex.model.ProductOffering;
import com.capgemini.cafex.model.ProductOfferingPrice;

/**
 * Prints an arbitrary invoice
 *
 */
public class App {

	public static void main(String[] args) {

		BillingService billSrv;
		CatalogService catalogSrv;

		ProductOffering cola = new ProductOffering();
		ProductOffering coffee = new ProductOffering();
		ProductOffering cheeseSandwich = new ProductOffering();
		ProductOffering steakSandwich = new ProductOffering();
		ProductOffering roundingSandwich = new ProductOffering();
		ProductOffering promoSandwich = new ProductOffering();

		// values from the examples given
		cola.setCharacteristic(ProductOffering.CHAR_CATEGORY, ProductOffering.VALUE_CAT_DRINK);
		cola.setCharacteristic(ProductOffering.CHAR_TEMPERATURE, ProductOffering.VALUE_TEMP_COLD);
		cola.setDescription("Cola - Cold");

		coffee.setCharacteristic(ProductOffering.CHAR_CATEGORY, ProductOffering.VALUE_CAT_DRINK);
		coffee.setCharacteristic(ProductOffering.CHAR_TEMPERATURE, ProductOffering.VALUE_TEMP_HOT);
		coffee.setDescription("Coffee - Hot");

		cheeseSandwich.setCharacteristic(ProductOffering.CHAR_CATEGORY, ProductOffering.VALUE_CAT_FOOD);
		cheeseSandwich.setCharacteristic(ProductOffering.CHAR_TEMPERATURE, ProductOffering.VALUE_TEMP_COLD);
		cheeseSandwich.setDescription("Cheese Sandwich - Cold");

		steakSandwich.setCharacteristic(ProductOffering.CHAR_CATEGORY, ProductOffering.VALUE_CAT_FOOD);
		steakSandwich.setCharacteristic(ProductOffering.CHAR_TEMPERATURE, ProductOffering.VALUE_TEMP_HOT);
		steakSandwich.setDescription("Steak Sandwich - Hot");

		steakSandwich.setCharacteristic(ProductOffering.CHAR_CATEGORY, ProductOffering.VALUE_CAT_FOOD);
		steakSandwich.setCharacteristic(ProductOffering.CHAR_TEMPERATURE, ProductOffering.VALUE_TEMP_HOT);
		steakSandwich.setDescription("Steak Sandwich - Hot");

		catalogSrv = new CatalogServiceImpl();

		catalogSrv.addProductOffering(cola, new ProductOfferingPrice(cola.getProductOfferingId(), .5d));
		catalogSrv.addProductOffering(coffee, new ProductOfferingPrice(coffee.getProductOfferingId(), 1d));
		catalogSrv.addProductOffering(cheeseSandwich,
				new ProductOfferingPrice(cheeseSandwich.getProductOfferingId(), 2d));
		catalogSrv.addProductOffering(steakSandwich,
				new ProductOfferingPrice(steakSandwich.getProductOfferingId(), 4.5d));
		catalogSrv.addProductOffering(roundingSandwich,
				new ProductOfferingPrice(roundingSandwich.getProductOfferingId(), 7.33d));
		catalogSrv.addProductOffering(promoSandwich,
				new ProductOfferingPrice(promoSandwich.getProductOfferingId(), 10d));

		// 10% service charge on order totals level if the order has food items
		OCC occ = new OCC(10d);
		occ.setCharacteristicName(ProductOffering.CHAR_CATEGORY);
		occ.setCharacteristicValue(ProductOffering.VALUE_CAT_FOOD);
		occ.setDescription("Service charge of 10%");
		catalogSrv.addOCC(occ);
		
		billSrv = new BillingServiceImpl(catalogSrv);

		Order order = new Order();
		order.addOrderLineItem(new OrderLineItem(2, cheeseSandwich.getProductOfferingId()));
		order.addOrderLineItem(new OrderLineItem(1, steakSandwich.getProductOfferingId()));
		order.addOrderLineItem(new OrderLineItem(10, cola.getProductOfferingId()));
		
		Invoice invoice = billSrv.createInvoice(order);
		System.out.println(invoice);

	}
}
