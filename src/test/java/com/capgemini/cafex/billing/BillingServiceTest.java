package com.capgemini.cafex.billing;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.cafex.catalog.CatalogService;
import com.capgemini.cafex.catalog.CatalogServiceImpl;
import com.capgemini.cafex.model.Invoice;
import com.capgemini.cafex.model.OCC;
import com.capgemini.cafex.model.OCCApplicationLevel;
import com.capgemini.cafex.model.OCCCalculationType;
import com.capgemini.cafex.model.OCCItem;
import com.capgemini.cafex.model.Order;
import com.capgemini.cafex.model.OrderLineItem;
import com.capgemini.cafex.model.ProductOffering;
import com.capgemini.cafex.model.ProductOfferingPrice;

public class BillingServiceTest {
	
	private BillingService billSrv;
	private CatalogService catalogSrv;
	
	ProductOffering cola = new ProductOffering();
	ProductOffering coffee = new ProductOffering();
	ProductOffering cheeseSandwich = new ProductOffering();
	ProductOffering steakSandwich = new ProductOffering();
	ProductOffering roundingSandwich = new ProductOffering();
	ProductOffering promoSandwich = new ProductOffering();
	
	{
		// values from the examples given		
		cola.setCharacteristic(ProductOffering.CHAR_CATEGORY, ProductOffering.VALUE_CAT_DRINK);
		cola.setCharacteristic(ProductOffering.CHAR_TEMPERATURE, ProductOffering.VALUE_TEMP_COLD);
		cola.setDescription("Cola - Cold");
		
		coffee.setCharacteristic(ProductOffering.CHAR_CATEGORY, ProductOffering.VALUE_CAT_DRINK);
		coffee.setCharacteristic(ProductOffering.CHAR_TEMPERATURE, ProductOffering.VALUE_TEMP_HOT);
		coffee.setDescription("Coffee - Hot");
		
		cheeseSandwich.setCharacteristic(ProductOffering.CHAR_CATEGORY, ProductOffering.VALUE_CAT_FOOD);
		cheeseSandwich.setCharacteristic(ProductOffering.CHAR_TEMPERATURE, ProductOffering.VALUE_TEMP_COLD);
		cheeseSandwich.setDescription("Cheese Sandwich - Cold");
		
		steakSandwich.setCharacteristic(ProductOffering.CHAR_CATEGORY, ProductOffering.VALUE_CAT_FOOD);
		steakSandwich.setCharacteristic(ProductOffering.CHAR_TEMPERATURE, ProductOffering.VALUE_TEMP_HOT);
		steakSandwich.setDescription("Steak Sandwich - Hot");
		
		steakSandwich.setCharacteristic(ProductOffering.CHAR_CATEGORY, ProductOffering.VALUE_CAT_FOOD);
		steakSandwich.setCharacteristic(ProductOffering.CHAR_TEMPERATURE, ProductOffering.VALUE_TEMP_HOT);
		steakSandwich.setDescription("Steak Sandwich - Hot");
		
		roundingSandwich.setCharacteristic(ProductOffering.CHAR_CATEGORY, "rounding");
		roundingSandwich.setCharacteristic(ProductOffering.CHAR_TEMPERATURE, ProductOffering.VALUE_TEMP_HOT);
		roundingSandwich.setDescription("Rounding Sandwich - Hot");
		
		promoSandwich.setCharacteristic(ProductOffering.CHAR_CATEGORY, "promo");
		promoSandwich.setCharacteristic(ProductOffering.CHAR_TEMPERATURE, ProductOffering.VALUE_TEMP_HOT);
		promoSandwich.setDescription("Promotion Sandwich - Hot");

	}

	
	@Before
	public void setUp() throws Exception {
		this.catalogSrv = new CatalogServiceImpl();
		
		this.catalogSrv.addProductOffering(cola, new ProductOfferingPrice(cola.getProductOfferingId(), .5d));
		this.catalogSrv.addProductOffering(coffee, new ProductOfferingPrice(coffee.getProductOfferingId(), 1d));
		this.catalogSrv.addProductOffering(cheeseSandwich,  new ProductOfferingPrice(cheeseSandwich.getProductOfferingId(), 2d));
		this.catalogSrv.addProductOffering(steakSandwich, new ProductOfferingPrice(steakSandwich.getProductOfferingId(), 4.5d));
		this.catalogSrv.addProductOffering(roundingSandwich, new ProductOfferingPrice(roundingSandwich.getProductOfferingId(), 7.33d));
		this.catalogSrv.addProductOffering(promoSandwich, new ProductOfferingPrice(promoSandwich.getProductOfferingId(), 10d));
		
		// 10% service charge on order totals level
		OCC occ = new OCC(10d);
		occ.setCharacteristicName(ProductOffering.CHAR_CATEGORY);
		occ.setCharacteristicValue(ProductOffering.VALUE_CAT_FOOD);
		occ.setDescription("Service charge of 10%");
		this.catalogSrv.addOCC(occ);
		
		// 3% discount for rounding errors sandwitch on order level
		occ = new OCC(-3d);
		occ.setCharacteristicName(ProductOffering.CHAR_CATEGORY);
		occ.setCharacteristicValue("rounding");
		this.catalogSrv.addOCC(occ);
		
		// 3.5 absolute discount for promo sandwitch on order item level
		occ = new OCC(-3.5d);
		occ.setApplicationLevel(OCCApplicationLevel.ORDER_LINE_ITEM);
		occ.setCalculationType(OCCCalculationType.ABSOLUTE);
		occ.setCharacteristicName(ProductOffering.CHAR_CATEGORY);
		occ.setCharacteristicValue("promo");
		this.catalogSrv.addOCC(occ);
		
		this.billSrv = new BillingServiceImpl(this.catalogSrv);
		
	}
	
	/**
	 * Pass in a list of purchased items that produces a total bill
	 */
	@Test
	public void testCanProduceAnInvoiceForAnItem() {
		Order order = new Order();
		order.addOrderLineItem(new OrderLineItem(coffee.getProductOfferingId()));
		Invoice invoice = this.billSrv.createInvoice(order);
		assertEquals(1d, invoice.getSubTotal(), 0d);
		assertEquals(1d, invoice.getTotal(), 0d);
	}
	
	/**
	 * Pass in a list of purchased items that produces a total bill
	 */
	@Test
	public void testCanProduceAnInvoiceForManyItems() {
		Order order = new Order();
		order.addOrderLineItem(new OrderLineItem(coffee.getProductOfferingId()));
		order.addOrderLineItem(new OrderLineItem(cola.getProductOfferingId()));
		
		Invoice invoice = this.billSrv.createInvoice(order);
		assertEquals(1.5d, invoice.getSubTotal(), 0d);
		assertEquals(1.5d, invoice.getTotal(), 0d);
	}
	
	/**
	 * Pass in a list of purchased items that produces a total bill
	 */
	@Test
	public void testCanProduceAnInvoiceForItemsWithQuantity() {
		Order order = new Order();
		order.addOrderLineItem(new OrderLineItem(7, coffee.getProductOfferingId()));
		order.addOrderLineItem(new OrderLineItem(10, cola.getProductOfferingId()));
		
		Invoice invoice = this.billSrv.createInvoice(order);
		assertEquals(12d, invoice.getSubTotal(), 0d);
		assertEquals(12d, invoice.getTotal(), 0d);
	}
	
	/**
	 * When all purchased items are drinks no service charge is applied
	 */
	@Test
	public void testNoChargeIsAppliedForDrinks() {
		Order order = new Order();
		order.addOrderLineItem(new OrderLineItem(7, coffee.getProductOfferingId()));
		order.addOrderLineItem(new OrderLineItem(10, cola.getProductOfferingId()));
		
		Invoice invoice = this.billSrv.createInvoice(order);
		assertEquals(invoice.getTotal(), invoice.getSubTotal(), 0d);
	}
	
	/**
	 * When purchased items include any food apply a service charge of 10% to the total bill (rounded to 2 decimal places)
	 */
	@Test
	public void testAChargeIsAppliedForFood() {
		Order order = new Order();
		order.addOrderLineItem(new OrderLineItem(2, cheeseSandwich.getProductOfferingId()));
		order.addOrderLineItem(new OrderLineItem(1, steakSandwich.getProductOfferingId()));
		
		Invoice invoice = this.billSrv.createInvoice(order);
		
		// 2 * 2 + 4.5
		assertEquals(8.5d, invoice.getSubTotal(), 0d);
		
		// 2 * 2 + 4.5 + 0.85
		assertEquals(9.35d, invoice.getTotal(), 0d);
	}
	
	/**
	 * When purchased items include any food apply a service charge of 10% to the total bill (rounded to 2 decimal places)
	 */
	@Test
	public void testAChargeIsAppliedForFoodAndDrinks() {
		Order order = new Order();
		order.addOrderLineItem(new OrderLineItem(2, cheeseSandwich.getProductOfferingId()));
		order.addOrderLineItem(new OrderLineItem(1, steakSandwich.getProductOfferingId()));
		order.addOrderLineItem(new OrderLineItem(10, cola.getProductOfferingId()));
		
		Invoice invoice = this.billSrv.createInvoice(order);
		
		// 2 * 2 + 4.5 + 5
		assertEquals(13.5d, invoice.getSubTotal(), 0d);
		
		// 2 * 2 + 4.5 + 5 + 1.35
		assertEquals(14.85d, invoice.getTotal(), 0d);
	}
	
	/**
	 * When purchased items include any food apply a service charge of 10% to the total bill (rounded to 2 decimal places)
	 */
	@Test
	public void test2DecimalRounding() {
		Order order = new Order();
		order.addOrderLineItem(new OrderLineItem(1, roundingSandwich.getProductOfferingId()));
		
		Invoice invoice = this.billSrv.createInvoice(order);
		
		// 7.33 - (3 * 7.33 / 100) = 7.33 - 0.2199 = 7.1101
		assertEquals(7.11d, invoice.getTotal(), 0d);
	}
	
	/**
	 * When purchased items include any food apply a service charge of 10% to the total bill (rounded to 2 decimal places)
	 */
	@Test
	public void test2LineItemDiscounts() {
		Order order = new Order();
		order.addOrderLineItem(new OrderLineItem(1, promoSandwich.getProductOfferingId()));
		
		Invoice invoice = this.billSrv.createInvoice(order);
		for (OCCItem item : invoice.getOccItems()) {
			assertNotNull(item.getInvoiceLineItemId());
		}
		
		// 10 - 3.5
		assertEquals(6.5d, invoice.getTotal(), 0d);
	}


}